module.exports = {
  extends: ['airbnb', 'plugin:prettier/recommended', 'prettier/flowtype', 'prettier/react'],
  plugins: ['prettier'],

  rules: {
    'prettier/prettier': ['error'],
    'react/jsx-props-no-spreading': ['off'],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/no-array-index-key': 0,
    'no-console': 0,
    indent: ['error', 2],
    'jsx-a11y/label-has-associated-control': 'off',
  },
  globals: {
    window: true,
    document: false,
  },
  env: {
    jest: true,
  },
};
