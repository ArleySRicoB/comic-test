/* eslint-disable import/no-extraneous-dependencies */
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/api/:number/info.0.json', (req, res) => {
  const ramdomParam = req.params.number;
  axios
    .get(`http://xkcd.com/${ramdomParam}/info.0.json`, {
      headers: { 'Access-Control-Allow-Origin': '*' },
    })
    .then((response) => res.status(200).send(response.data))
    .catch((error) => res.status(404).send(error.message));
});

app.listen(3001, () => console.log('Express server is running on localhost:3001'));
