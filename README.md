# Comic-test

Comic-test es un proyecto para obtener, mostrar y puntuar comics de manera aleatoria, además, ver los comics puntuados.

## Configuraciones

### Pre-requisitos

- **Node**

### Instalar dependencias

En la raíz de proyecto, instale las dependencias de node con:

```bash
$ npm install
```

### Añadir variables de entorno

Para poder correr la aplicación, cree el archivo `.env` para añadir las variables de entorno.

Para una fácil creación, duplique el archivo `.env.example` y renombrelo.

```bash
.env.example   ->    .env
```

| Nombre  | Descripción                     |
| ------- | ------------------------------- |
| API_URL | URL para obtener la información |

### Correr la aplicación

En la raíz de proyecto, inicie el servidor webpack y el proxy con:

```bash
$ npm run dev
```

### Interactuar con la aplicación

Abra el navegador en la siguiente URL: http://localhost:8080/

La aplicación tendrá una barra de navegación con dos opciones:

- **Home**: Ver un comic aleatorio, aquí se puede puntuar el comic dando click sobre las estrellas y buscar otro comic de manera aleatoria.
- **Rated Comics**: Ver la lista de comics que fueron puntuados en home.
