import React from 'react';
import PropTypes from 'prop-types';

const Comic = ({ comic, handleScore, score, handleReloadPage }) => (
  <section className="comic-section container animated fadeIn">
    <div className="row d-flex justify-content-center text-center">
      <h1 className="w-100">{comic.title}</h1>
      <div className="col-12 px-4">
        <hr />
        <img src={comic.img} alt="comic-img" />
        <h6 className="pt-2">
          Published on: {comic.day}/{comic.month}/{comic.year}
        </h6>
      </div>
      <div className="col-12 col-md-8 col-lg-6 pt-4">
        <p className="mx-2">{comic.alt}</p>
      </div>
      <div className="col-12 star-button pt-5">
        <div>
          <h6>Rate this comic:</h6>
        </div>
        <div className="d-flex flex-row-reverse justify-content-center flex-wrap">
          <button type="button" onClick={() => handleScore(5)}>
            <i className={`fa fa-star px-0 px-sm-1 ${score >= 5 ? 'checked' : 'not-checked'}`} />
          </button>
          <button type="button" onClick={() => handleScore(4)}>
            <i className={`fa fa-star px-0 px-sm-1 ${score >= 4 ? 'checked' : 'not-checked'}`} />
          </button>
          <button type="button" onClick={() => handleScore(3)}>
            <i className={`fa fa-star px-0 px-sm-1 ${score >= 3 ? 'checked' : 'not-checked'}`} />
          </button>
          <button type="button" onClick={() => handleScore(2)}>
            <i className={`fa fa-star px-0 px-sm-1 ${score >= 2 ? 'checked' : 'not-checked'}`} />
          </button>
          <button type="button" onClick={() => handleScore(1)}>
            <i className={`fa fa-star px-0 px-sm-1 ${score >= 1 ? 'checked' : 'not-checked'}`} />
          </button>
        </div>
      </div>
    </div>

    <div className="w-100 d-flex justify-content-center mt-5">
      <button
        type="button"
        onClick={() => handleReloadPage()}
        className="btn btn-lg btn-outline-primary btn-block col-11 col-md-6 col-lg-4"
      >
        <i className="fa fa-random px-3" />
        Look for another comic
      </button>
    </div>
  </section>
);

Comic.propTypes = {
  comic: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]).isRequired,
  handleScore: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
  handleReloadPage: PropTypes.func.isRequired,
};

export default Comic;
