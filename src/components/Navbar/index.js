import React from 'react';

const NavBar = () => {
  const route = window.location.pathname;
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarOptions"
        aria-controls="navbarOptions"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarOptions">
        <ul className="navbar-nav mr-auto">
          <li className={`nav-item ${route === '/' && 'active'}`}>
            <a className="nav-link" href="/">
              Home <span className="sr-only">(current)</span>
            </a>
          </li>
          <li className={`nav-item ${route === '/rated-comics' && 'active'}`}>
            <a className="nav-link" href="/rated-comics">
              Rated Comics
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
