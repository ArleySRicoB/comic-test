import React from 'react';
import PropTypes from 'prop-types';

const RatedComics = ({ comicsList }) => {
  return (
    <div className="container text-center animated fadeIn">
      <h1>Rated Comics</h1>
      <table className="table mt-5">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Published on</th>
            <th scope="col">Rate (1 - 5)</th>
          </tr>
        </thead>
        <tbody>
          {comicsList.map((comic, index) => (
            <tr key={index}>
              <td>{comic.title}</td>
              <td>
                {comic.day}/{comic.month}/{comic.year}
              </td>
              <td>{comic.score}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

RatedComics.propTypes = {
  comicsList: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]).isRequired,
};

export default RatedComics;
