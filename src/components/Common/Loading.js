import React from 'react';

const Loading = () => {
  return (
    <div className="d-flex justify-content-center pt-5">
      <div className="loader" />
    </div>
  );
};

export default Loading;
