/* eslint-disable no-undef */
import React from 'react';
import RatedComicsComponent from '../components/RatedComics';

const RatedComics = () => {
  const comicsList = JSON.parse(localStorage.getItem('comicsRated'));
  return <RatedComicsComponent comicsList={comicsList} />;
};

export default RatedComics;
