import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ComicComponent from '../components/Comic';
import Loading from '../components/Common/Loading';
import Error from '../components/Common/Error';
import saveComicRate from '../utils';

const Comic = () => {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [score, setScore] = useState(0);
  const [random, setRandom] = useState(Math.floor(Math.random() * 2001));

  const handleScore = (newScore) => {
    setScore(newScore);
    saveComicRate(data, newScore);
  };

  const handleReloadPage = () => {
    setRandom(Math.floor(Math.random() * 2001));
    setScore(0);
  };

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      await axios(`${process.env.API_URL}/${random}/info.0.json`)
        .then((response) => {
          setData(response.data);
        })
        .catch(() => {
          setIsError(true);
        });
      setIsLoading(false);
    };

    fetchData();
  }, [random]);

  if (isError) return <Error error="Unable to get Comics" />;
  if (isLoading) return <Loading />;
  return (
    <ComicComponent
      comic={data}
      handleScore={handleScore}
      score={score}
      handleReloadPage={handleReloadPage}
    />
  );
};

export default Comic;
