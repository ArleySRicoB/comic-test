import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Comic from './containers/Comic';
import RatedComics from './containers/RatedComics';
import NavBar from './components/Navbar';
import Error from './components/Common/Error';

const App = () => {
  return (
    <Router>
      <Route component={NavBar} />
      <Switch>
        <Route exact path="/" component={Comic} />
        <Route exact path="/rated-comics" component={RatedComics} />
        <Route component={() => <Error error="Page not found" />} />
      </Switch>
    </Router>
  );
};

export default App;
