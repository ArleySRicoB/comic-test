/* eslint-disable no-undef */
const saveComicRate = (data, score) => {
  const ratedComics = JSON.parse(localStorage.getItem('comicsRated'));
  if (!ratedComics || ratedComics === []) {
    localStorage.setItem('comicsRated', JSON.stringify([{ ...data, score }]));
  } else {
    const comicIndex = ratedComics.findIndex((item) => item.num === data.num);
    if (comicIndex === -1) ratedComics.push({ ...data, score });
    else ratedComics[comicIndex].score = score;
    localStorage.setItem('comicsRated', JSON.stringify(ratedComics));
  }
};

export default saveComicRate;
