/* eslint-disable import/extensions */
import React from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/jquery/dist/jquery.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import './styles.scss';
import App from './App';

render(<App />, document.getElementById('app'));
